function quicksort(d, lewy, prawy) {
    var i, j, x;
    i = j = lewy;

    while (i < prawy) {
        //TODO
        if (d[i] <= d[prawy]) { // pivotem jest element o indeksie [prawy]
            x = d[j];
            d[j] = d[i];
            d[i] = x;
            j++;
        }
        i++;
    }

    x = d[j];
    d[j] = d[prawy];
    d[prawy] = x;

    if (lewy < j - 1)
        quicksort(d, lewy, j - 1);
    if (j + 1 < prawy)
        quicksort(d, j + 1, prawy);
} // koniec algorytmu



