var array = [];
var arr = [];

for (var i = 0; i < 5199; i++) {
    arr.push(Math.round(Math.random() * 100));
}
//
var wynik = document.querySelector("#wyniki");
//

// losowanie
var losujBtn = document.querySelector("#losuj");
var numberOfRandom = document.querySelector("#ileLiczb");
var random = [];

losujBtn.addEventListener('click', function () {
    array = [];
    for (var i = 0; i <= numberOfRandom.value - 1; i++) {
        array.push(parseInt(Math.random() * arr.length));
    }
    wynik.innerHTML = "Wylosowane numery: " + array;
    return array;
});

// zwykłe sort

var sortBtn = document.querySelector("#sortNoormal");
var q1 = document.querySelector("#q1");

sortBtn.addEventListener('click', function () {
    var time = yProfiler();
    time.start();
    var sorting = array.slice();
    

    sorting.sort(function (a, b) {
        return a - b;
    });
    console.log(sorting, array);

    wynik.innerHTML = "Posortowane numery: " + sorting;
    q1.innerHTML = "Czas: " + time.stop() + 'ms';

});


// sort babelkowe

var sortBabBtn = document.querySelector("#sortBab");
var q2 = document.querySelector("#q2");
var s2 = document.querySelector("#s2");
var countBa = 0;

sortBabBtn.addEventListener('click', function () {
    var time = yProfiler();
    time.start();

    var sortingBab = array.slice();


    n = sortingBab.length;
    do {
        countBa++;
        for (var i = 0; i < n - 1; i++) {
            
            if (sortingBab[i] > sortingBab[i + 1]) {
                
                var tmp = sortingBab[i];
                sortingBab[i] = sortingBab[i + 1];
                sortingBab[i + 1] = tmp;
            }
            
        }
        n = n - 1;
    } while (n > 1);

    wynik.innerHTML = "Posortowane numery bąbelkowo: " + sortingBab;
    q2.innerHTML = "Czas: " + time.stop() + 'ms';
    s2.innerHTML = "<br>Pętli: " + countBa;

});

// sort marge

var sortMBtn = document.querySelector("#sortMer");
var q3 = document.querySelector("#q3");
var s3 = document.querySelector("#s3");

sortMBtn.addEventListener('click', function () {
    var time = yProfiler();
    time.start();

    var sortingMarge = array.slice();;
    

    var sorted = mergeSort(sortingMarge);

    wynik.innerHTML = "Posortowane numery w marge: " + sorted;
    q3.innerHTML = "Czas: " + time.stop() + 'ms';
    s3.innerHTML = "<br>Pętli: " + countM;

});

// sort swap

var sortSwap = document.querySelector("#sortSwap");
var q4 = document.querySelector("#q4");
var s4 = document.querySelector("#s4");


sortSwap.addEventListener('click', function () {
    var time = yProfiler();
    time.start();

    var sortingSwap = array.slice();;
  
    heapsort(sortingSwap);

    wynik.innerHTML = "Posortowane numery w swap: " + sortingSwap;
    q4.innerHTML = "Czas: " + time.stop() + 'ms';
    s4.innerHTML = "<br>Pętli: " + countS;

});

// sort insert

var sortInsert = document.querySelector("#sortInsert");
var q5 = document.querySelector("#q5");
var s5 = document.querySelector("#s5");


sortInsert.addEventListener('click', function () {
    var time = yProfiler();
    time.start();

    var sortingInsert = array.slice();;
   

    insertionSort(sortingInsert);

    wynik.innerHTML = "Posortowane numery w sort insert: " + sortingInsert;
    q5.innerHTML = "Czas: " + time.stop() + 'ms';
    s5.innerHTML = "<br>Pętli: " + countI;

});

// sort quick

var sortQuick = document.querySelector("#sortQuick");
var q6 = document.querySelector("#q6");
var s6 = document.querySelector("#s6");


sortQuick.addEventListener('click', function () {
    var time = yProfiler();
    time.start();

    var sortingInsert = array.slice();;
   

    quicksort(sortingInsert, 0, sortingInsert.length - 1);

    wynik.innerHTML = "Posortowane numery w sort quick: " + sortingInsert;
    q6.innerHTML = "Czas: " + time.stop() + 'ms';
    s6.innerHTML = "<br>Pętli: " + countQ;
    

});


//var time = yProfiler();
//time.start();
//mergeSort();
//heapsort();
//insertionSort();
//quicksort();
//alert(time.stop());