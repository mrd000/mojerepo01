function mergeSort(arr)
{
    if (arr.length < 2)
        return arr;

    var middle = parseInt(arr.length / 2);
    var left = arr.slice(0, middle);
    var right = arr.slice(middle, arr.length);

    return merge(mergeSort(left), mergeSort(right));
}

var countM = 0;
function merge(left, right)
{
    var result = [];
    

    while (left.length && right.length) {
        //TODO
        countM ++;

        if (left[0] <= right[0]) {
            result.push(left.shift());
        } else {
            result.push(right.shift());
        }
    }
    while (left.length)
        result.push(left.shift());

    while (right.length)
        result.push(right.shift());
    
    return result;
}

// swap
var countS = 0;
function swap(a, i, j) {
    //TODO
    countS++;
    
    var tmp = a[i];
    a[i] = a[j];
    a[j] = tmp;
}
function max_heapify(a, i, length) {
    while (true) {
        var left = i * 2 + 1;
        var right = i * 2 + 2;
        var largest = i;
        if (left < length && a[left] > a[largest]) {
            largest = left;
        }
        if (right < length && a[right] > a[largest]) {
            largest = right;
        }
        if (i == largest) {
            break;
        }
        swap(a, i, largest);
        i = largest;
    }
}
function heapify(a, length) {
    for (var i = Math.floor(length / 2); i >= 0; i--) {
        max_heapify(a, i, length);
    }
}
function heapsort(a) {
    heapify(a, a.length);
    for (var i = a.length - 1; i > 0; i--) {
        swap(a, i, 0);
        max_heapify(a, 0, i - 1);
    }
}

// insert
var countI = 0;
function insertionSort(unsortedList) {
    var len = unsortedList.length;
    for (var i = 1; i < len; i++) {
        var tmp = unsortedList[i]; //Copy of the current element. 
        /*Check through the sorted part and compare with the number in tmp. If large, shift the number*/
        for (var j = i - 1; j >= 0 && (unsortedList[j] > tmp); j--) {
            //Shift the number
            //TODO
            countI++;
            unsortedList[j + 1] = unsortedList[j];
        }
        //Insert the copied number at the correct position
        //in sorted part. 
        unsortedList[j + 1] = tmp;
    }
}

// quick
var countQ = 0;
function quicksort(d, lewy, prawy) {
    var i, j, x;
    i = j = lewy;

    while (i < prawy) {
        //TODO
        countQ++;
        
        if (d[i] <= d[prawy]) { // pivotem jest element o indeksie [prawy]
            x = d[j];
            d[j] = d[i];
            d[i] = x;
            j++;
        }
        i++;
    }

    x = d[j];
    d[j] = d[prawy];
    d[prawy] = x;

    if (lewy < j - 1)
        quicksort(d, lewy, j - 1);
    if (j + 1 < prawy)
        quicksort(d, j + 1, prawy);
} // koniec algorytmu


// czas 

var yProfiler = function ()
{
    var nTimeStart = 0,
            nTimeStop = 0,
            nLastDuration = -1,
            oDate = null;

    function fStart()
    {
        oDate = new Date();
        nTimeStart = oDate.getTime();
        return nTimeStart;
    }
    function fStop()
    {
        oDate = new Date();
        nTimeStop = oDate.getTime();
        nLastDuration = nTimeStop - nTimeStart;
        return nLastDuration;
    }
    function fTime()
    {
        // nie wystartowano jeszcze :/
        if (!nTimeStart)
        {
            return false;
        }
        // nie skonczono jeszcze
        if (!nTimeStop)
        {
            fStop();
        }
        if (nLastDuration < 0 || isNaN(nLastDuration))
        {
            nLastDuration = nTimeStop - nTimeStart;
        }
        return nLastDuration;
    }
    function fGetStart()
    {
        return (nTimeStart) ? nTimeStart : false;
    }
    function fGetStop()
    {
        return (nTimeStop) ? nTimeStop : false;
    }
    function fLoop(func, n)
    {
        fStart();
        for (i = 0; i < n; i++)
        {
            func();
        }
        fStop();
        return nLastDuration;
    }

    // konstruktor:
    fStart(); // domyslnie startuje
    return {
        start: fStart,
        stop: fStop,
        time: fTime,
        getStart: fGetStart,
        getStop: fGetStop,
        loop: fLoop
    };
};